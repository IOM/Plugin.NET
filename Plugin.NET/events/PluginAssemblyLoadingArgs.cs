﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginNET.events
{
    /// <summary>
    /// Assembly加载前事件的参数
    /// </summary>
    public class PluginAssemblyLoadingArgs : PluginEventArgs
    {
        /// <summary>
        /// DLL文件的完整路径
        /// </summary>
        public string FileName { get; internal set; }
    }
}
